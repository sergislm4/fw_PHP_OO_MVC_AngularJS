var watupper = angular.module('watupper',['ngRoute', 'ngMaterial', 'ngAnimate', 'ui.bootstrap', 'ngMdIcons', 'ngCookies']);
watupper.config(['$routeProvider',
    function ($routeProvider) {
        // console.log("Hola");
        $routeProvider
                // Home
                .when("/", {
                    templateUrl: "frontend/modules/main/view/main.view.html", 
                    controller: "mainCtrl",
                    resolve: {
                        main: function (services) {
                            return services.get('main', 'load_more', "4");
                        },
                        load_more: function (services) {
                            return services.get('main', 'load_more', "6");
                        }
                    }
                })

                // Contact
                .when("/contact", {
                    templateUrl: "frontend/modules/contact/view/contact.view.html", 
                    controller: "contactCtrl",
                    resolve: {
                        products: function (services) {
                            return services.get('main', 'list_products');
                        }
                    }
                })

                // Products
                .when("/products", {
                    templateUrl: "frontend/modules/products/view/products.view.html",
                    controller: "productsCtrl",
                    resolve: {
                        products: function (services) {
                            return services.get('main', 'list_products');
                        }
                    }

                })

                .when("/products/:id/", {
                    templateUrl: "frontend/modules/products/view/details.view.html",
                    controller: "detailsCtrl",
                    resolve: {
                        data: function (services, $route) {
                            return services.get('main', 'getProduct', $route.current.params.id);
                        },
                        ratings: function (services, $route) {
                            return services.get('main', 'getRatingsWhere', $route.current.params.id);
                        }
                    }
                })

                .when("/products/where/:type/", {
                    templateUrl: "frontend/modules/products/view/products.view.html",
                    controller: "productsCtrl",
                    resolve: {
                        products: function (services, $route) {
                            return services.get('main', 'getProductWhere', $route.current.params.type);
                        }
                    }
                })

                // .when("/myevents", {
                //     templateUrl: "frontend/modules/events/view/main.view.html",
                //     controller: "eventsfavoriteCtrl",
                //     resolve: {
                //         events: function (services, $route, cookiesService ) {
                //             var user = cookiesService.GetCredentials();
                //             return services.post('events', 'getfavorites', JSON.stringify({usuario: user.usuario}));
                //         }
                //     }
                // })

                //Signup
                .when("/login/signup/", {
                    templateUrl: "frontend/modules/login/view/signup.view.html",
                    controller: "signupCtrl"
                })

                //Activar Usuario
                .when("/login/verify/:token", {
                    templateUrl: "frontend/modules/main/view/main.view.html",
                    controller: "verifyCtrl"
                })

                //Restore
                .when("/login/restore/", {
                    templateUrl: "frontend/modules/login/view/restore.view.html",
                    controller: "restoreCtrl"
                })

                //ChangePass
                .when("/login/change_pass/:token", {
                    templateUrl: "frontend/modules/login/view/changepass.view.html",
                    controller: "changepassCtrl"
                })

                //Perfil
                .when("/login/profile/", {
                    templateUrl: "frontend/modules/login/view/profile.view.html",
                    controller: "profileCtrl",
                    resolve: {
                        user: function (services, cookiesService) {
                            var user = cookiesService.GetCredentials();
                            // console.log(user.token.user_id);
                            if (user) {
                                return services.post('login', 'profile_filler', {user: user.token.user_id});
                            }
                            return false;
                        }
                    }
                })

                .when("/login/my_ratings/", {
                    templateUrl: "frontend/modules/login/view/my_ratings.view.html",
                    controller: "my_ratingsCtrl",
                    resolve: {
                        ratings: function (services, cookiesService, $rootScope) {
                            var user = cookiesService.GetCredentials();
                            // console.log(user.token.user_id);
                            if (user) {
                                return services.post('login', 'my_ratings', {user: user.token.user_id});
                            }
                            return false;
                        }
                    }
                })

                // else 404
                .otherwise("/", {
                    templateUrl: "frontend/modules/main/view/main.view.html",
                    controller: "mainCtrl",
                    resolve: {
                        main: function (services) {
                            return services.get('main', 'load_more', "4");
                        }
                    }
                });
    }]);
    
    watupper.run(function($rootScope, $mdDialog, services, cookiesService, $location) {
        var user = cookiesService.GetCredentials();
        if (user) {
            $rootScope.access = false;
            $rootScope.logged = true;
            services.post('login', 'display_menu', {token: user.token.user_id})
                .then(function (response) {
                    // console.log(response[0].email);
                    $rootScope.avatar = response[0].avatar;
                    $rootScope.email = response[0].email.split('@')[0];
                    $rootScope.email_ratings = response[0].email;
            });
        } else {
            $rootScope.logged = false;
            $rootScope.access = true;
        }
        $rootScope.global_modal = function(ev) {
            // alert("Hola");
            $mdDialog.show({
                controller: "loginCtrl",
                templateUrl: 'frontend/modules/login/view/login_modal.view.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false
            });
        };

        $rootScope.logout = function() {
            cookiesService.ClearCredentials();

            $rootScope.access = true;
            $rootScope.logged = false;

            $rootScope.avatar = 'email';
            $rootScope.nombre = 'email';
            $location.path('/');
        };
    });

