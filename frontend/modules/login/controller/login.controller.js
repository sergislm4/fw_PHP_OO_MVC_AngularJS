watupper.controller('loginCtrl', function ($scope, $route, services, $rootScope, $mdDialog, initService, CommonService, $location, $timeout, cookiesService, social_signin) {
    $scope.form = {
        email: "",
        password: ""
    };

    $scope.login = function () {
        var data = {"email": $scope.form.email, "pass": $scope.form.password};
        // console.log(data);
        data = JSON.stringify(data);

        services.post("login", "login", {user: data}).then(function (response) {
            // console.log(response.error);
            if (!response.error) {
                cookiesService.SetCredentials(response[0]);
                $mdDialog.cancel();
                initService.login();
                $location.path("/#");
            } else {
                if (response.data == 503)
                    CommonService.customToaster("Unexpected error! Try it later, our appologies.", '1000', 'top right', 'md-toast-error');
                else if (response.data == 404){
                    $location.path("/");
                    CommonService.customToaster("Unexpected error! Try it later, our appologies.", '1000', 'top right', 'md-toast-error');
                }else {
                    $scope.err = true;
                    $scope.errorpass = response.data;
                    $timeout(function () {
                        $scope.err = false;
                        $scope.errorpass = "";
                    }, 2000);
                }
            }
        });

    }

    $scope.google = function() {
        social_signin.google_login();
    };

    $scope.twitter = function() {
        social_signin.twitter_login();
    };

    $scope.github = function() {
        social_signin.github_login();
    };

    $scope.close = function(){
        $mdDialog.cancel();
    }
});

watupper.controller('signupCtrl', function ($scope, dateService, services, $location, $timeout, CommonService, $mdDialog, $filter) {
    $mdDialog.cancel();

    // this.myDate = new Date();

    // $scope.minDate = new Date(
    //     this.myDate.getFullYear() - 70,
    //     this.myDate.getMonth(),
    //     this.myDate.getDate()
    // );

    // $scope.maxDate = new Date(
    //     this.myDate.getFullYear() -18,
    //     this.myDate.getMonth(),
    //     this.myDate.getDate()
    // );

    $scope.signup = {
        inputEmail: "",
        inputPass: "",
        inputPass2: "",
        inputBirthdate: ""
    };


    $scope.error = function() {
        $scope.signup.email_error = "";
        $scope.signup.pass_error = "";
        $scope.signup.birthdate_error = "";
    };

    $scope.change_signup = function () {
        $scope.signup.email_error = "";
        $scope.signup.pass_error = "";
        $scope.signup.birthdate_error = "";
    };

    // $scope.signup.inputBirthdate = $scope.Birthdate
    // $scope.format = function(date) {
    //     $scope.signup.inputBirthdate = $filter('date')(new Date(date), 'MM/dd/yyyy')
    //     console.log($scope.signup.inputBirthdate);
    // };



    dateService.date($scope);

    $scope.format = function(date) {
        $scope.signup.inputBirthdate = $filter('date')(new Date(date), 'MM/dd/yyyy')
    };
    $scope.SubmitSignUp = function () {
        // console.log($scope.signup);
        var data = {"email": $scope.signup.inputEmail, "password": $scope.signup.inputPass, "password2": $scope.signup.inputPass2,
            "birthdate": $scope.signup.inputBirthdate};
        var data_users_JSON = JSON.stringify(data);
        console.log(data_users_JSON);
        services.post('login', 'register', {register_json:data_users_JSON}).then(function (response) {
            console.log(response);
            if (response.success) {
                $timeout(function () {
                    $location.path('/');
                    CommonService.customToaster("User signup has been successful, check your email to activate your new account.", '1000', 'top right', 'md-toast-done');
                }, 2000);
            } else {
                if (response.typeErr === "Email") {
                    $scope.AlertMessage = true;
                    $timeout(function () {
                        $scope.AlertMessage = false;
                    }, 5000);
                    $scope.signup.email_error = response.error;

                } else if (response.typeErr === "error") {
                    //console.log(response.error);
                    $scope.AlertMessage = true;
                    $timeout(function () {
                        $scope.AlertMessage = false;
                    }, 5000);
                    $scope.signup.email_error = response.error.email;
                    $scope.signup.pass_error = response.error.password;
                } else if (response.typeErr === "error_server"){
                    CommonService.customToaster("Server error, please try again later.", '1000', 'top right', 'md-toast-error');
                }
            }
        });
    };
});

watupper.controller('verifyCtrl', function (initService, $location, CommonService, $route, services, cookiesService) {
    var token = $route.current.params.token;
    services.get("login", "verify", token).then(function (response) {
        //console.log(response);
        //console.log(response.user[0].usuario);
        if (response.success) {
            CommonService.customToaster("Your account has been activated successfully.", '1000', 'top right', 'md-toast-done');
            initService.login();
            $location.path('/');
        } else {
            CommonService.customToaster("Server error, please try again later.", '1000', 'top right', 'md-toast-error');
            $location.path("/");
        }
    });
});

watupper.controller('restoreCtrl', function ($scope, services, $timeout, $location, CommonService, $mdDialog) {
    $scope.restore = {
        inputEmail: ""
    };

    $mdDialog.cancel();

    $scope.SubmitRestore = function () {
        var data = {"inputEmail": $scope.restore.inputEmail};
        var restore_form = JSON.stringify(data);

        services.post('login', 'process_restore', restore_form).then(function (response) {
            // console.log(response);
            response = response.split("|");
            $scope.message = response[1];
            if (response[0] == 'True') {
                $timeout(function () {
                    $scope.class = 'alert alert-success';
                    $location.path('/');
                    CommonService.customToaster($scope.message, '1000', 'top right', 'md-toast-done');
                }, 3000);
            } else {
                $timeout(function () {
                    $scope.class = 'alert alert-error';
                    $location.path('/');
                    CommonService.customToaster($scope.message, '1000', 'top right', 'md-toast-error');
                }, 3000);
            }
        });
    };
});

watupper.controller('changepassCtrl', function ($route, $scope, services, $location, CommonService) {
    var token = $route.current.params.token;
    $scope.changepass = {
        inputPassword: ""
    };

    $scope.SubmitChangePass = function () {
        var data = {"password": $scope.changepass.inputPassword, "token": token};
        var passw = JSON.stringify(data);

        services.put('login', 'update_pass', {password: passw}).then(function (response) {
            // console.log(response);
            if (response.success) {
                $location.path('/');
                CommonService.customToaster("Your password has been updated", '1000', 'top right', 'md-toast-done');
            } else {
                CommonService.customToaster("Server error, try again later!", '1000', 'top right', 'md-toast-error')
            }
        });
    };
});

watupper.controller('profileCtrl', function ($scope, dateService, initService, services, user, $location, CommonService,
load_pais_prov_poblac, $timeout, cookiesService, $filter, dateService) {
    // console.log(user);
    if (!user) {
        $location.path("/");
    }

    $scope.format = function(date) {
        $scope.birthdate = $filter('date')(new Date(date), 'MM/dd/yyyy')
    };

    dateService.date($scope);


    //llenar los campos del form_profile con scope
    $scope.user = user.user;
    $scope.drop = {
        msgClass: ''
    };
    $scope.inputPassword="";

    //disabled mail
    $scope.controlmail = false; //ng-disabled=false
    if (user.user.email)
        $scope.controlmail = true;

    //errors
    $scope.error = function() {
        $scope.user.name_error = "";
        $scope.user.surn_error = "";
        $scope.user.email_error = "";
        $scope.user.password_error = "";
        $scope.user.pais_error = "";
        $scope.user.prov_error = "";
        $scope.user.pob_error = "";
    };

    $scope.change_profile = function () {
        $scope.user.name_error = "";
        $scope.user.surn_error = "";
        $scope.user.email_error = "";
        $scope.user.password_error = "";
    };

    //rellenar pais, provincias y poblaciones
    load_pais_prov_poblac.load_pais()
    .then(function (response) {
        if(response.success){
            $scope.paises = response.datas;
        }else{
            $scope.AlertMessage = true;
            $scope.user.pais_error = "Error al recuperar la informacion de paises";
            $timeout(function () {
                $scope.user.pais_error = "";
                $scope.AlertMessage = false;
            }, 2000);
        }
    });

    $scope.resetPais = function () {
        if ($scope.user.pais.sISOCode == 'ES') {
            load_pais_prov_poblac.loadProvincia()
            .then(function (response) {
                if(response.success){
                    $scope.provincias = response.datas;
                }else{
                    $scope.AlertMessage = true;
                    $scope.user.prov_error = "Error al recuperar la informacion de provincias";
                    $timeout(function () {
                        $scope.user.prov_error = "";
                        $scope.AlertMessage = false;
                    }, 2000);
                }
            });
            $scope.poblaciones = null;
        } 
    };

    $scope.resetValues = function () {
        var datos = {idPoblac: $scope.user.provincia.id};
        load_pais_prov_poblac.loadPoblacion(datos)
        .then(function (response) {
            if(response.success){
                $scope.poblaciones = response.datas;
            }else{
                $scope.AlertMessage = true;
                $scope.user.pob_error = "Error al recuperar la informacion de poblaciones";
                $timeout(function () {
                    $scope.user.pob_error = "";
                    $scope.AlertMessage = false;
                }, 2000);
            }
        });
    };

    //dropzone
    $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=login&function=upload_avatar',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Server error",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {},
            'success': function (file, response) {
                // console.log(response);
                // response = JSON.parse(response);
                // console.log(response);
                if (response.resultado) {
                    $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
                    $('.msg').animate({'right': '300px'}, 300);

                    console.log(response.datos);
                    $scope.user.avatar = response.datos;

                    // initService.login();
                } else {
                    $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
                    $('.msg').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                if (file.xhr.response) {
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    var data = jQuery.parseJSON(file.xhr.response);
                    services.post("user", "delete_avatar", JSON.stringify({'filename': data}));
                }
            }
    }};

    $scope.submit = function () {
        var pais, prov, pob = null;
        if (!$scope.user.pais.sISOCode) { //el usuario no escoge pais
            pais = " ";
        }else{ //el usuario escoge pais
            pais = $scope.user.pais.sISOCode;
            if($scope.user.pais.sISOCode !== "ES"){
                prov = " ";
                pob = " ";
            }
        }

        if (!$scope.user.provincia.id) { //el usuario no escoge provincia
            prov = " ";
        }else{ //el usuario escoge provincia
            prov = $scope.user.provincia.id;
        }

        if (!$scope.user.poblacion.poblacion) { //el usuario no escoge poblacion
            pob = " ";
        }else{ //el usuario escoge poblacion
            pob = $scope.user.poblacion.poblacion;
        }

        //var data = JSON.stringify($scope.user);
        var data = {"email": $scope.user.email, "nombre": $scope.user.name,
        "apellidos": $scope.user.surnames, "password": $scope.inputPassword, "birthdate": $scope.birthdate, "pais": pais,
        "provincia": prov,"poblacion": pob, "avatar": $scope.user.avatar};
        var data1 = JSON.stringify(data);
        console.log(data);

        services.post("login", "modify", {mod_user_json: data1}).then(function (response) {
            //console.log(response);
            //console.log(response.user[0].usuario);

            //limpiar el avatar de :80
            // console.log(response);
            // var avatar = response.user[0].avatar;
            // var buscar = avatar.indexOf(":80");
            // if(buscar !== -1){
            //     var avatar = avatar.replace(":80", "");
            //     response.user[0].avatar = avatar;
            // }
    //         //console.log(response.user[0].avatar);

            if (response.success) {
                // cookiesService.SetCredentials(response.user[0]);
                initService.login();
                    $timeout(function () {
                        $location.path($location.path());
                        CommonService.customToaster("User profile updated successfully!", '1000', 'top right', 'md-toast-done');
                    }, 2000);
            } else {
                $timeout(function () {
                    CommonService.customToaster("Server error!", '1000', 'top right', 'md-toast-error');;
                }, 3000);
            }
        });
    };
});

watupper.controller('my_ratingsCtrl', function ($scope, ratings, $route) {
    console.log(ratings);
    $scope.ratings = ratings.ratings;

});