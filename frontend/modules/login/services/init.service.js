watupper.factory("initService", ['$location', '$rootScope', 'services', 'cookiesService',
function ($location, $rootScope, services, cookiesService) {
        var service = {};
        service.login = login;
        return service;
        
        $rootScope.access = false;
        $rootScope.logged = true;

        function login() {
            //al cargarse la pagina por primera vez, user es undefined
            var user = cookiesService.GetCredentials();
            // console.log(user);
            // console.log(user.token.user_id);
            if (user) {
                $rootScope.access = false;
                $rootScope.logged = true;
                services.post('login', 'display_menu', {token: user.token.user_id})
                    .then(function (response) {
                        // console.log(response);
                        $rootScope.avatar = response[0].avatar;
                        $rootScope.email = response[0].email.split('@')[0];
                });
            }
        }
}]);