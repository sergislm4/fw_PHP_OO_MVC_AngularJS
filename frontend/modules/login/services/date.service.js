watupper.factory("dateService",
function ($filter) {
	var service = {};
	service.date = date;
	return service;

	function date($scope){
		// console.log($scope);
		$scope.myDate = new Date();

	    $scope.minDate = new Date(
	        $scope.myDate.getFullYear() - 70,
	        $scope.myDate.getMonth(),
	        $scope.myDate.getDate()
	    );

	    $scope.maxDate = new Date(
	        $scope.myDate.getFullYear() -18,
	        $scope.myDate.getMonth(),
	        $scope.myDate.getDate()
	    );
	}

	$scope.format = function(date) {
		/* $scope.format function author: Joan Montes */
        $scope.birthdate = $filter('date')(new Date(date), 'MM/dd/yyyy')
    };
});