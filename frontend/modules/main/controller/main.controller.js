watupper.controller('mainCtrl', function ($scope, main, load_more) {
  	$scope.myInterval = 5000;
  	$scope.noWrapSlides = false;
  	$scope.active = 0;
  	$scope.slides = main.products;
  	// console.log($scope.slides);
  	$scope.dsblBtn = false;
  	$scope.load_more = load_more.products;
	$scope.product = $scope.load_more;
	$scope.limit = 3;
	
	console.log($scope.load_more);
	$scope.loadMore = function() {
	  var incremented = $scope.limit + 3;
	  $scope.limit = incremented > $scope.product.length ? $scope.product.length : incremented;
	  $scope.dsblBtn = true;
	};
});