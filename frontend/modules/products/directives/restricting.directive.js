watupper.directive('restrictField', function () {
    return {
    restrict: 'A',

    link: function($scope, $element) {
      $element.bind('input', function() {
        $(this).val($(this).val().replace(/(?:\b|')(ass|airhead|asshole|ass-kisser|bastard|dago|donkey|dork|nerd|fool|stupid|weirdo|rat)(?:\b|')/g, ''));
      });
    }
  };
});