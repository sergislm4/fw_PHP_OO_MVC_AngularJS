document.addEventListener("DOMContentLoaded", function(event) {
	var config = {
		apiKey: "AIzaSyARWDQ5EbIEB4CpfPD9g6yT9tI4FeLkicM",
		authDomain: "watupper-85be0.firebaseapp.com",
		databaseURL: "https://watupper-85be0.firebaseio.com",
		projectId: "watupper-85be0",
		storageBucket: "watupper-85be0.appspot.com",
		messagingSenderId: "946012518323"
	};

	firebase.initializeApp(config);

	document.querySelector('input#login').addEventListener("click", function(){
		login();
	});

	document.getElementById('google_login').addEventListener('click', function() {
		google_login();
	});

	document.getElementById('twitter_login').addEventListener('click', function() {
		twitter_login();
	});

	document.getElementById('github_login').addEventListener('click', function() {
		github_login();
	});

});

function login () {
		var email = document.querySelector("input#inputEmail").value;
		var password = document.querySelector("input#inputPass").value;

		$(".alert").remove();
		if (!email) {
	        $("input#inputEmail").focus().after("<span class='alert alert-warning'>Empty Field!</span>");
	        value = false;
	    } else {
	        if (!password) {
	            $("input#inputPass").focus().after("<span class='alert alert-warning'>Empty Field!</span>");
	            value = false;
	        } else
	            value = true;
	    }

	    var data = {"email": email, "pass": password};
	    var login_JSON = JSON.stringify(data);
	    // console.log(login_JSON);
	    if (value) {
	    	$.post(amigable("?module=login&function=login"), {login_json: login_JSON}, 
	        function (response) {
	            console.log(response);
	            if (!response.error) {
	                //create session cookies
	                Tools.createCookie("token", response[0]['user_id']);
	                window.location.href = amigable("?module=main");
	            } else {
	                if (response.data == 503)
	                    window.location.href = amigable("?module=main&function=begin&param=503");
	                else
	                	$('#resultMessage').attr('disabled', false);
	                    $("#resultMessage").focus().after("<span class='alert alert-warning'>" + response.data + "</span>");
	            }
	        }, "json").fail(function (xhr, textStatus, errorThrown) {
	            // console.log(xhr.responseText);
	            if (xhr.status === 0) {
	                alert('Not connect: Verify Network.');
	            } else if (xhr.status === 404) {
	                alert('Requested page not found [404]');
	            } else if (xhr.status === 500) {
	                alert('Internal Server Error [500].');
	            } else if (textStatus === 'parsererror') {
	                alert('Requested JSON parse failed.');
	            } else if (textStatus === 'timeout') {
	                alert('Time out error.');
	            } else if (textStatus === 'abort') {
	                alert('Ajax request aborted.');
	            } else {
	                alert('Uncaught Error: ' + xhr.responseText);
	            }
	        });
    	}
}

function google_login () {
	$(".alert").remove();
	
	var provider = new firebase.auth.GoogleAuthProvider();

    var authService = firebase.auth();

    // manejador de eventos para loguearse
    
     authService.signInWithPopup(provider)
        .then(function(result) {
            // console.log('Hemos autenticado al usuario ', result.user);
            // console.log(result.user.displayName);
            // console.log(result.user.email);
            // console.log(result.user.photoURL);
            console.log(result.user.uid);
            // console.log(result.credential.accessToken);
            var data = {"token":result.user.uid, "email": result.user.email, "avatar": result.user.photoURL};
	    	var login_JSON = JSON.stringify(data);
	    	// console.log(login_JSON);
	    
	    	$.post(amigable("?module=login&function=social_signin"), {user: login_JSON}, 
	        function (response) {
	            // console.log(response);
	            if (!response.error) {
	                //create session cookies
	                Tools.createCookie("token", response[0]['user_id']);
	                window.location.href = amigable("?module=main");
	            } else {
	                if (response.data == 503)
	                    window.location.href = amigable("?module=main&function=begin&param=503");
	                else
	                	$('#resultMessage').attr('disabled', false);
	                    $("#resultMessage").focus().after("<span class='alert alert-warning'>" + response.data + "</span>");
	            }
	        }, "json").fail(function (xhr, textStatus, errorThrown) {
	            // console.log(xhr.responseText);
	            if (xhr.status === 0) {
	                alert('Not connect: Verify Network.');
	            } else if (xhr.status === 404) {
	                alert('Requested page not found [404]');
	            } else if (xhr.status === 500) {
	                alert('Internal Server Error [500].');
	            } else if (textStatus === 'parsererror') {
	                alert('Requested JSON parse failed.');
	            } else if (textStatus === 'timeout') {
	                alert('Time out error.');
	            } else if (textStatus === 'abort') {
	                alert('Ajax request aborted.');
	            } else {
	                alert('Uncaught Error: ' + xhr.responseText);
	            }
	        });
        })
        .catch(function(error) {
        	$('#resultMessage').attr('disabled', false);
            $("#resultMessage").focus().after("<span class='alert alert-warning'>" + error + "</span>");
        });
    
} 

function twitter_login () {
	$(".alert").remove();

	var provider = new firebase.auth.TwitterAuthProvider();
    var authService = firebase.auth();

	authService.signInWithPopup(provider)
		.then(function(result) {
		    //var token = result.credential.accessToken;
		    //var secret = result.credential.secret;
		    // console.log(result.user);
		    // console.log(result.user.displayName);
		    // console.log(result.user.email);
		    // console.log(result.user.photoURL);
		    // console.log(result.credential.accessToken);
		    var data = {"token":result.user.uid, "email": result.user.displayName, "avatar": result.user.photoURL};
	    	var login_JSON = JSON.stringify(data);
	    	// console.log(login_JSON);
	    
	    	$.post(amigable("?module=login&function=social_signin"), {user: login_JSON}, 
	        function (response) {
	            // console.log(response);
	            if (!response.error) {
	                //create session cookies
	                Tools.createCookie("token", response[0]['user_id']);
	                window.location.href = amigable("?module=main");
	            } else {
	                if (response.data == 503)
	                    window.location.href = amigable("?module=main&function=begin&param=503");
	                else
	                	$('#resultMessage').attr('disabled', false);
	                    $("#resultMessage").focus().after("<span class='alert alert-warning'>" + response.data + "</span>");
	            }
	        }, "json").fail(function (xhr, textStatus, errorThrown) {
	            // console.log(xhr.responseText);
	            if (xhr.status === 0) {
	                alert('Not connect: Verify Network.');
	            } else if (xhr.status === 404) {
	                alert('Requested page not found [404]');
	            } else if (xhr.status === 500) {
	                alert('Internal Server Error [500].');
	            } else if (textStatus === 'parsererror') {
	                alert('Requested JSON parse failed.');
	            } else if (textStatus === 'timeout') {
	                alert('Time out error.');
	            } else if (textStatus === 'abort') {
	                alert('Ajax request aborted.');
	            } else {
	                alert('Uncaught Error: ' + xhr.responseText);
	            }
	        });
		})
		.catch(function(error) {
			$('#resultMessage').attr('disabled', false);
	    	$("#resultMessage").focus().after("<span class='alert alert-warning'>" + error.message + "</span>");
	  	});


} 

function github_login () {
	$(".alert").remove();

	var provider = new firebase.auth.GithubAuthProvider();
	var authService = firebase.auth();

	authService.signInWithPopup(provider)
		.then(function(result) {
		    //var token = result.credential.accessToken;
		    //var secret = result.credential.secret;
		    // console.log(result.user);
		    // console.log(result.user.displayName);
		    // console.log(result.user.email);
		    // console.log(result.user.photoURL);
		    // console.log(result.credential.accessToken);
		    var data = {"token":result.user.uid, "email": result.user.email, "avatar": result.user.photoURL};
	    	var login_JSON = JSON.stringify(data);
	    	// console.log(login_JSON);
	    
	    	$.post(amigable("?module=login&function=social_signin"), {user: login_JSON}, 
	        function (response) {
	            // console.log(response);
	            if (!response.error) {
	                //create session cookies
	                Tools.createCookie("token", response[0]['user_id']);
	                window.location.href = amigable("?module=main");
	            } else {
	                if (response.data == 503)
	                    window.location.href = amigable("?module=main&function=begin&param=503");
	                else
	                	$('#resultMessage').attr('disabled', false);
	                    $("#resultMessage").focus().after("<span class='alert alert-warning'>" + response.data + "</span>");
	            }
	        }, "json").fail(function (xhr, textStatus, errorThrown) {
	            // console.log(xhr.responseText);
	            if (xhr.status === 0) {
	                alert('Not connect: Verify Network.');
	            } else if (xhr.status === 404) {
	                alert('Requested page not found [404]');
	            } else if (xhr.status === 500) {
	                alert('Internal Server Error [500].');
	            } else if (textStatus === 'parsererror') {
	                alert('Requested JSON parse failed.');
	            } else if (textStatus === 'timeout') {
	                alert('Time out error.');
	            } else if (textStatus === 'abort') {
	                alert('Ajax request aborted.');
	            } else {
	                alert('Uncaught Error: ' + xhr.responseText);
	            }
	        });
		})
		.catch(function(error) {
			$('#resultMessage').attr('disabled', false);
	    	$("#resultMessage").focus().after("<span class='alert alert-warning'>" + error.message + "</span>");
	  	});
} 