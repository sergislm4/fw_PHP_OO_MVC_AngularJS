$(document).ready(function () {
    if (!getCookie('keyword')) {
        // console.log("good");
        search_empty();
    } else {
        //console.log("hola");
        $('input#keyword').val((getCookie('keyword')));
        count_product(getCookie('keyword'));
        setCookie('keyword', '', 1);
    }

    $("input#Submit").click(function (e) {
        //$('.pagination').val('');
        var keyword = '';
        keyword = $('input#keyword').val();
        // console.log(keyword);
        var v_keyword = validate_search(keyword);
        
        if (!v_keyword) {
            search_empty();
        } else {
            setCookie('keyword', keyword, 1);
        }
        location.reload(true);
        e.preventDefault(); 
    });

    $.post(amigable("?module=main&function=autocomplete_products"), {'autocomplete': true}, function (data, status) {
        // console.log(data);
        var json = JSON.parse(data);
        var nom_productos = json.nom_productos;
        // console.log(nom_productos);
        var suggestions = new Array();
        $.each(nom_productos, function(index, object) {
            suggestions.push(object.title);
        })
        
        // console.log(suggestions);

        $("input#keyword").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                // alert(ui.item.label);
                var keyword = '';
                keyword = ui.item.label;
                count_product(keyword);
            }
        });
    }).fail(function (xhr) {
        $("div#results").html('Unexpected error...');
    });

});

function search_empty() {
    $.post(amigable("?module=main&function=num_pages_products"), {'num_pages': true}, function (data, status) {
        // console.log(data);
        var json = JSON.parse(data);
        var pages = json.pages;
        load_defaults(1);
        $(".pagination").bootpag({
            total: pages,
            page: 1,
            maxVisible: 3,
            next: 'next',
            prev: 'prev'
        }).on("page", function (e, num) {
            //console.log(num);
            e.preventDefault();
            load_defaults(num);
            reset();
        });
        reset();
    }).fail(function (xhr) {
        $("div#results").html('Unexpected error...');
        $('.pagination').html('');
        reset();
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}

function validate_search(search_value) {
    if (search_value.length > 0) {
        var regexp = /^[a-zA-Z0-9 .]*$/;
        return regexp.test(search_value);
    }
    return false;
}

function count_product(keyword) {
    $.post(amigable("?module=main&function=count_products"), {'count_product': keyword}, function (data, status) {
        var json = JSON.parse(data);
        var num_products = json.num_products;
        if (num_products == 0) {
            $("div#results").html('Unexpected error...');
            $('.pagination').html('');
            reset();
        }
        if (num_products == 1) {
            search_product(keyword);
        }
        if (num_products > 1) {
            load_defaults(1, keyword);
        }
    }).fail(function () {
        $("div#results").html('Unexpected error...');
        $('.pagination').html('');
        reset();
    });
}

function reset() {
    var $div = $("div#results");
    $div.html("");
}

function load_defaults(pages, keyword){

    if ((!keyword && !pages)) {

        var get_url = amigable("?module=main&function=list_products");
        $.get( get_url, function( data ) {
            // console.log(data);
            var $div = $("div#results");
            $div.html("");
            var message = JSON.parse(data);
            // console.log(message);
            if(message){
               $.each(message, function(index, object) {
                    var $row = $('<div class="col-md-4 text-center">' 
                    + '<div class="product">'
                    + '<div class="product-grid" style="background-image:url('+object.img+');">'
                    + '<div class="inner">'
                    + '<p>'
                    + '<a href="#" class="icon buy"><i class="icon-shopping-cart"></i></a>'
                    + '<a href="../../main/read_product/'+object.title+'" class="icon read"><i class="icon-eye"></i></a>'
                    + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="desc">'
                    + '<h3><a href="../../main/read_product/'+object.title+'" class="read">'+object.title+'</a></h3>'
                    + '<span class="price">'+object.price+'€</span>'
                    + '</div>'
                    + '</div>'
                    + '</div>').appendTo($div);
                })
            }else if(message==1){  
                $div.html('Wrong data!');

            }else{
                $div.html('Unexpected error...');
            };
        });
    } else if(pages && !keyword) {
        // console.log("hola");
        var post_url = amigable("?module=main&function=obtain_products");
        $.post( post_url, {'page_num': pages},  function( data ) {
            // console.log(data);
            var $div = $("div#results");
            $div.html("");
            var message = JSON.parse(data);
            // console.log(message);
            if(message){
               $.each(message, function(index, object) {
                    var $row = $('<div class="col-md-4 text-center">' 
                    + '<div class="product">'
                    + '<div class="product-grid" style="background-image:url('+object.img+');">'
                    + '<div class="inner">'
                    + '<p>'
                    + '<a href="#" class="icon buy"><i class="icon-shopping-cart"></i></a>'
                    + '<a href="../../main/read_product/'+object.title+'" class="icon read"><i class="icon-eye"></i></a>'
                    + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="desc">'
                    + '<h3><a href="../../main/read_product/'+object.title+'" class="read">'+object.title+'</a></h3>'
                    + '<span class="price">'+object.price+'€</span>'
                    + '</div>'
                    + '</div>'
                    + '</div>').appendTo($div);
                })
            }else if(message==1){  
                $div.html('Wrong data!');

            }else{
                $div.html('Unexpected error...');
            };
        });
    } else {
        var post_url = amigable("?module=main&function=obtain_products");
        $.post( post_url, {'page_num': pages, 'keyword': keyword},  function( data ) {
            // console.log(data);
            var $div = $("div#results");
            $div.html("");
            var message = JSON.parse(data);
            // console.log(message);
            if(message){
               $.each(message, function(index, object) {
                    var $row = $('<div class="col-md-4 text-center">' 
                    + '<div class="product">'
                    + '<div class="product-grid" style="background-image:url('+object.img+');">'
                    + '<div class="inner">'
                    + '<p>'
                    + '<a href="#" class="icon buy"><i class="icon-shopping-cart"></i></a>'
                    + '<a href="../../main/read_product/'+object.title+'" class="icon read"><i class="icon-eye"></i></a>'
                    + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="desc">'
                    + '<h3><a href="../../main/read_product/'+object.title+'" class="read">'+object.title+'</a></h3>'
                    + '<span class="price">'+object.price+'€</span>'
                    + '</div>'
                    + '</div>'
                    + '</div>').appendTo($div);
                })
            }else if(message==1){  
                $div.html('Wrong data!');

            }else{
                $div.html('Unexpected error...');
            };
        });

    };

}

function search_not_empty(keyword) {
    
    $.post(amigable("?module=main&function=num_pages_products"), {'num_pages': true, 'keyword': keyword}, function (data, status) {
        //console.log(data);
        var json = JSON.parse(data);
        var pages = json.pages;
        var count = 0;

        load_defaults(1, keyword);

        if (pages !== 0) {
            $('.pagination').val('');
            $(".pagination").bootpag({
                total: pages,
                page: 1,
                maxVisible: 3,
                next: 'next',
                prev: 'prev'
            }).on("page", function (e, num) {
                e.preventDefault();
                load_defaults(num, keyword);
                reset();
            });
        } else {
            $("div#results").html('Unexpected error...');
            $('.pagination').html('');
            reset();
        }
        reset();
    }).fail(function (xhr) {
        $("div#results").html('Unexpected error...');
        $('.pagination').html('');
        reset();
    });
}

function search_product(keyword) {
    $.post(amigable("?module=main&function=obtain_products"), {'increment': true,'keyword': keyword}, function (data, status) {
        var message = JSON.parse(data);
        reset();
        if(message){
               $.each(message, function(index, object) {
                    var $row = $('<center><div class="col-md-12">'
                    + '<h3 class="title"><b> '+object.title+'</b></h3>'
                    + '<hr />'
                    + '<img src="'+object.img+'"></img>'
                    + '</div>'
                    + '<br /><br />'
                    + '<div class="col-md-12 product_content">'
                    + '<hr />'
                    + '</center>'
                    + '<p>'
                    + '<h5><em>#'+object.reference+'</em></h5>'
                    + '</p>'
                    + '<div class="rating">'
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '<span class="glyphicon glyphicon-star"></span>   '
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '</div>'
                    + '<p>'
                    + '<h4 class="category"><b>Type:</b> '+object.alimentation+'</h4>'
                    + '</p>'
                    + '<p>'
                    + '<h4 class="category">'+object.details+'</h4>'
                    + '<br />'
                    + '<div>'
                    + '<h2 class="category"><span class="glyphicon glyphicon-tags"></span>&nbsp;<b>'+object.price+'€</b></h2>'
                    + '</div>'
                    + '<br />'
                    + '</div>'
                    + '</div>'
                    + '<br />'
                    + '<div id="map"></div>'
                    + '<br />'
                    + '<div class="btn-ground">'
                    + '<a class="btn btn-default" href="../../main/all_products/"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Return</a>'
                    + '</div>').appendTo("div#results");
                    $.post(amigable("?module=contact&function=list_store"),{'store': object.store}, function (data) {
                        var map = new google.maps.Map(document.getElementById('map'), {
                            center: {lat: -34.397, lng: 150.644},
                            zoom: 6
                          });
                          var infoWindow = new google.maps.InfoWindow({map: map});

                          // Try HTML5 geolocation.
                          if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                              var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                              };

                              infoWindow.setPosition(pos);
                              infoWindow.setContent('You');
                              map.setCenter(pos);
                            }, function() {
                              handleLocationError(true, infoWindow, map.getCenter());
                            });
                          } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                          }
                        // console.log(data);
                        var message = JSON.parse(data);
                        // console.log(message);
                        $.each(message, function(index, object) {
                          var contentString = '<div class="inner"">'+
                              '<div id="siteNotice">'+
                              '</div>'+
                              '<h1 id="firstHeading" class="firstHeading">'+object.storename+'</h1>'+
                              '<div id="bodyContent">'+
                              '<p><b>'+object.storename+'</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sodales nisl nec volutpat mollis. Cras finibus cursus ultrices. Curabitur egestas.</p>'+
                              '</div>'+
                              '</div>';

                          var infowindow = new google.maps.InfoWindow({
                            content: contentString
                          });

                          var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(object.lat, object.lng),
                            map: map,
                            title: object.storename
                          });
                          marker.addListener('click', function() {
                            infowindow.open(map, marker);
                          });
                        })
                      });
                })
            }else if(message==1){  
                $("div#results").html('Wrong data!');

            }else{
                $("div#results").html('Unexpected error...');
            };
    }).fail(function (xhr) {
        $("div#results").html('Unexpected error...');
        $('.pagination').html('');
        reset();
    });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
}