$(document).ready(function () {
    slider(4);
	load_more(3);
});

function slider(items){
    $.post(amigable("?module=main&function=load_more"), {'items': items}, function (data, status) {
            // console.log("hola1");
            // console.log(data);
            // var $div = $("ul#slider");
            // $div.html("");
            var message = JSON.parse(data);
            // console.log(message);
            if(message){
                i=0;
               $.each(message, function(index, object) {
                    i++;
                    var $div = $("li#slider"+i+"");
                    $div.css('background-image', 'url(' + object.img + ')');
                    $div.html("");
                    var $row = $('<div class="overlay-gradient"></div>'
                    + '<div class="container">'
                    + '<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">'
                    + '<div class="slider-text-inner">'
                    + '<div class="desc">'
                    + '<span class="price">'+object.price+'€</span>'
                    + '<h2>'+object.title+'</h2>'
                    + '<p>'+object.details+'</p>'
                    + '<p><a href="../main/read_product/'+object.title+'" class="btn btn-primary btn-outline btn-lg">Purchase Now</a></p>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>').appendTo($div);
                })
            }else if(message==1){  
                $div.html('Wrong data!');

            }else{
                $div.html('Unexpected error...');
            };
        });
}

function load_more(items){
	if (items==3) {
		// console.log("hola");
        $.post(amigable("?module=main&function=load_more"), {'items': items}, function (data, status) {
        	// console.log("hola1");
            // console.log(data);
            var $div = $("div#load");
            $div.html("");
            var message = JSON.parse(data);
            // console.log(message);
            if(message){
               $.each(message, function(index, object) {
                    var $row = $('<div class="col-md-4 text-center">' 
                    + '<div class="product">'
                    + '<div class="product-grid" style="background-image:url('+object.img+');">'
                    + '<div class="inner">'
                    + '<p>'
                    + '<a href="#" class="icon buy"><i class="icon-shopping-cart"></i></a>'
                    + '<a href="../main/read_product/'+object.title+'" class="icon read"><i class="icon-eye"></i></a>'
                    + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="desc">'
                    + '<h3><a href="../main/read_product/'+object.title+'" class="read">'+object.title+'</a></h3>'
                    + '<span class="price">'+object.price+'€</span>'
                    + '</div>'
                    + '</div>'
                    + '</div>').appendTo($div);
                })
               var $row = $('<hr /><p align="center"><a  onclick="load_more(6);" id="load_more" class="btn btn-primary btn-outline">More Products</a></p>').appendTo($div);
            }else if(message==1){  
                $div.html('Wrong data!');

            }else{
                $div.html('Unexpected error...');
            };
        });
	} else {
    	var post_url = amigable("?module=main&function=load_more");
        $.post( post_url, {'items': items},  function( data ) {
            // console.log(data);
            var $div = $("div#load");
            $div.html("");
            var message = JSON.parse(data);
            // console.log(message);
            if(message){
               $.each(message, function(index, object) {
                    var $row = $('<div class="col-md-4 text-center">' 
                    + '<div class="product">'
                    + '<div class="product-grid" style="background-image:url('+object.img+');">'
                    + '<div class="inner">'
                    + '<p>'
                    + '<a href="#" class="icon buy"><i class="icon-shopping-cart"></i></a>'
                    + '<a href="../main/read_product/'+object.title+'" class="icon read"><i class="icon-eye"></i></a>'
                    + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="desc">'
                    + '<h3><a href="../main/read_product/'+object.title+'" class="read">'+object.title+'</a></h3>'
                    + '<span class="price">'+object.price+'€</span>'
                    + '</div>'
                    + '</div>'
                    + '</div>').appendTo($div);
                })
               var $row = $('<hr /><p align="center"><a href="../main/all_products/" class="btn btn-primary btn-outline">More Products</a></p>').appendTo($div);
            }else if(message==1){  
                $div.html('Wrong data!');

            }else{
                $div.html('Unexpected error...');
            };
        });
	}
}    