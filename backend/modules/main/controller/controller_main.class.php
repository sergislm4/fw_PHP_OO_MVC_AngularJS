<?php
class controller_main {
    function __construct() {
        $_SESSION['module'] = "main";
    }



    function list_products() {
        if($_GET){
                $rdo = loadModel(MODEL_MAIN, "main_model", "main_model", "list_products");
                $list = array();
                foreach ($rdo as $row) {
                    array_push($list, $row);
                }
                $arrArguments['products'] = $list;
                $arrArguments['success'] = true;
                echo json_encode($arrArguments);
                exit();
            }
    }

    function getProductWhere() {
        try {
            $arrValue = loadModel(MODEL_MAIN, "main_model", "select_type", $_GET['param']);
        } catch (Exception $e) {
            $arrValue = false;
        }

        if ($arrValue) {
            $arrArguments['products'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function getRatingsWhere() {
        try {
            $arrValue = loadModel(MODEL_MAIN, "main_model", "select_ratings", $_GET['param']);
        } catch (Exception $e) {
            $arrValue = false;
        }

        if ($arrValue) {
            $arrArguments['ratings'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function autocomplete_products() {
        if ((isset($_POST["autocomplete"])) && ($_POST["autocomplete"] === "true")) {
            $nom_productos = loadModel(MODEL_MAIN, "main_model", "live_search");
            
            if ($nom_productos) {
                $jsondata["nom_productos"] = $nom_productos;
                echo json_encode($jsondata);
                exit;
            } else {
                echo json_encode(1);
            }
        }
    }

    function count_products() {
        if (($_POST["count_product"])) {
            $result = filter_string($_POST["count_product"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }
            
            
            $result = loadModel(MODEL_MAIN, "main_model", "total_rows_like", $criteria);
            $total_rows = $result[0]['total'];
            
            
            if ($total_rows) {
                $jsondata["num_products"] = $total_rows;
                echo json_encode($jsondata);
                exit;
            } else {
                echo json_encode(1);
            }
        }
    }

    function num_pages_products() {
        if ((isset($_POST["num_pages"])) && ($_POST["num_pages"] === "true")) {
            $item_per_page = 3;

            if (isset($_POST["keyword"])) {
                $result = filter_string($_POST["keyword"]);
                if ($result['resultado']) {
                    $criteria = $result['datos'];
                } else {
                    $criteria = '';
                }
            } else {
                $criteria = '';
            }
            
            
            $arrValue = loadModel(MODEL_MAIN, "main_model", "total_products", $criteria);
            $get_total_rows = $arrValue[0]["total"]; //total records
            $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
            
            if ($get_total_rows) {
                $jsondata["pages"] = $pages;
                echo json_encode($jsondata);
                exit;
            } else {
                echo json_encode(1);
            }
        }
    }

    function getProduct() {
        try {
            $arrValue = loadModel(MODEL_MAIN, "main_model", "obtain_product_ref", $_GET['param']);
        } catch (Exception $e) {
            $arrValue = false;
        }

        if ($arrValue) {
            $arrArguments['products'] = $arrValue[0];
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function obtain_products() {
        //filter to $_POST["page_num"]
        if (isset($_POST["page_num"])) {
            $result = filter_num_int($_POST["page_num"]);
            if ($result['resultado']) {
                $page_number = $result['datos'];
            }
        } else {
            $page_number = 1;
        }

        //filter $_GET["keyword"]
        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }
        } else {
            $criteria = '';
        }

        if ($_POST["increment"] = true) {
            $result = filter_string($_POST["keyword"]);
            $arrcriteria = $result['datos'];
            $VAL = loadModel(MODEL_MAIN, "main_model", "increment", $arrcriteria);
        }

        //to paging when we have not empty keyword
        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria1 = $result['datos'];
            } else {
                $criteria1 = '';
            }
        } else {
            $criteria1 = '';
        }

        if (isset($_POST["keyword"])) {
            $criteria = $criteria1;
        }

       
        $item_per_page = 3;
        $position = (($page_number - 1) * $item_per_page);

        $arrArgument = array(
            'position' => $position,
            'item_per_page' => $item_per_page,
            'criteria' => $criteria
        );
        $arrValue = loadModel(MODEL_MAIN, "main_model", "page_products", $arrArgument);
        $list = array();
        foreach ($arrValue as $row) {
            array_push($list, $row);
        }
        echo json_encode($list);
        exit();
        
    }
    function load_more() {
        // echo json_encode("value");
        // exit();
        // if ($_POST["items"]) {
            // $criteria = $_POST["items"];
            // echo json_encode($criteria);
            // exit();
            
            $arrValue = loadModel(MODEL_MAIN, "main_model", "load_more", $_GET['param']);
            $list = array();
            foreach ($arrValue as $row) {
                array_push($list, $row);
            }
            $arrArguments['products'] = $list;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
            exit();
        // }
    }

    function list_store() {
        if (isset($_POST["store"])) {
            $criteria=$_POST["store"];
            $store = loadModel(MODEL_MAIN, "main_model", "list_store", $criteria);
            
            if ($store) {
                $jsondata = $store;
                echo json_encode($jsondata);
                exit;
            } else {
                echo json_encode(1);
            }
        }
    }

}