$(document).ready(function () {
  // var city = {lat: 38.8151804, lng: -0.606651};
  // var map = new google.maps.Map(document.getElementById('map'), {
  //   zoom: 6,
  //   center: city
  // });
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 6
  });
  var infoWindow = new google.maps.InfoWindow({map: map});

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('You');
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }

  $.get(amigable("?module=contact&function=list_stores"), function (data) {
    // console.log(data);
    var message = JSON.parse(data);
    // console.log(message);
    $.each(message, function(index, object) {
      var contentString = '<div class="inner"">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<h1 id="firstHeading" class="firstHeading">'+object.storename+'</h1>'+
          '<div id="bodyContent">'+
          '<p><b>'+object.storename+'</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sodales nisl nec volutpat mollis. Cras finibus cursus ultrices. Curabitur egestas.</p>'+
          '</div>'+
          '</div>';

      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });

      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(object.lat, object.lng),
        map: map,
        title: object.storename
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });
    })
  });
});

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
}