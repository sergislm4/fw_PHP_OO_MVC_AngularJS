<?php   
    class controller_contact { 
        
        public function __construct() {
            $_SESSION['module'] = "contact";
        }

        public function view_contact() {
            require_once(VIEW_PATH_INC."header.php"); 
			require_once(VIEW_PATH_INC."menu.php");
            
            loadView(CONTACT_VIEW_PATH, 'contact.html');
            
            require_once(VIEW_PATH_INC."footer.html");
        }
        
        public function process_contact() {
            if($_POST['token'] === "contact_form"){
                
                //////////////// Mail to user
                $arrArgument = array(
									'type' => 'contact',
									'token' => '',
									'inputName' => $_POST['inputName'],
									'inputEmail' => $_POST['inputEmail'],
									'inputSubject' => $_POST['inputSubject'],
									'inputMessage' => $_POST['inputMessage']
								);
				try{
					enviar_email($arrArgument);
                    echo "|true|Your contact mail has been sent correctly!";
               	//////////////// Mail to admin
	                $arrArgument = array(
										'type' => 'admin',
										'token' => '',
										'inputName' => $_POST['inputName'],
										'inputEmail' => $_POST['inputEmail'],
										'inputSubject' => $_POST['inputSubject'],
										'inputMessage' => $_POST['inputMessage']
									);
					enviar_email($arrArgument);
					exit();
				} catch (Exception $e) {
					echo "|false|Service error. Try again later...";
					exit();
				}		
            }else{
                echo "|false|Service error. Try again later...";
                exit();
            }
        }

	    function list_stores() {
	        if($_GET){
	                $rdo = loadModel(MODEL_CONTACT, "contact_model", "list_stores");
	                $list = array();
	                foreach ($rdo as $row) {
	                    array_push($list, $row);
	                }
	                echo json_encode($list);
	                exit();
	            }
	    }
    }